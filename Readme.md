# Notification system

### How to start

**Run Kafka**
```
docker-compose up -d
```

**Run the application**
```
mvn clean install
mvn spring-boot:run
```

**List topics**
```
winpty docker exec -it notifications-kafka-1 kafka-topics.sh --list --bootstrap-server kafka:9092
```

Result in log
```
email
slack
sms
```

**Send messages**

- with Postman
```
import kafka.postman_collection.json in Postman and run the collection
```

- with [newman](https://www.npmjs.com/package/newman#getting-started) 

```
newman run ./kafka.postman_collection.json
```

Expected result in log

```
Received sms message: Message{from='Me', to='You', message='Hi by SMS'}
Send by SMS: Message{from='Me', to='You', message='Hi by SMS'}

Received e-mail message: Message{from='Me', to='You', message='Hi by E-mail'}
Send by E-mail: Message{from='Me', to='You', message='Hi by E-mail'}

Received slack message: Message{from='Me', to='You', message='Hi by Slack'}
Send by Slack: Message{from='Me', to='You', message='Hi by Slack'}
```


### Design && Implementation:

To implement the task an Event-Driven Architecture has been chosen with [Apache Kafka](https://kafka.apache.org/). As well as Rest API for receiving messages.

Benefits of this approach are listed in articles like:
- [Event-Driven Architecture: Getting Started with Kafka (Part 1)](https://blog.ippon.tech/event-driven-architecture-getting-started-with-kafka-part-1/)
- [Event-Driven Architecture: Getting Started with Kafka (Part 2)](https://blog.ippon.tech/event-driven-architecture-getting-started-with-kafka-part-2/)
- and more

The Java framework used is [Spring Boot](https://spring.io/projects/spring-boot):
- Spring Rest API for receiving requests
- and [Spring for Apache Kafka](https://spring.io/projects/spring-kafka) for processing notifications

[Docker Compose](https://docs.docker.com/compose/) to containerize the [Apache ZooKeeper](https://zookeeper.apache.org/) && [Apache Kafka](https://kafka.apache.org/) mitigating known issues (e.g. issues with Kafka on Windows platforms)

### Documentation:

**Package** `controller`
- A Notification controller is implemented for handling incoming requests. It provides path variable for specifying the message type.

**Package** `service`
- A Message Sender service is implemented for sending SMS, E-mail and Slack messages. The [Strategy Pattern](https://www.tutorialspoint.com/design_pattern/strategy_pattern.htm) is used.
- A Message Producer service is implemented for sending messages to Kafka broker in transactional mode. The [Strategy Pattern](https://www.tutorialspoint.com/design_pattern/strategy_pattern.htm) is used by functional interfaces.
- A Message Listener service is implemented for consuming messages from Kafka broker.

**Package** `model`
- A Message POJO/DTO for processing messages.

**Package** `config`
- A Topic config for creating Kafka topics on the fly.
- A Producer config for setting up a Kafka Template for sending messages to Kafka broker.
- A Consumer config for setting up a Kafka Listener Factory.
- A Service config for setting up the services from the `service` package.

**Package** `common`
- Defined Constants used in the project.

**Resources**
- application.yml - defined spring kafka bootstrap-servers, producer and consumer props. All defined here for easily overriding by other Spring/Java profiles. 
