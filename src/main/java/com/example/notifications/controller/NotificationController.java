package com.example.notifications.controller;

import com.example.notifications.model.Message;
import com.example.notifications.service.MessageProducer;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;

@RestController
@RequestMapping("/api/notification")
public class NotificationController {

    private final MessageProducer producer;

    public NotificationController(MessageProducer producer){
        this.producer = producer;
    }

    @PostMapping("/send/{type}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void send(@PathVariable String type, @RequestBody Message message) {
        producer.send(type, message);
    }
}
