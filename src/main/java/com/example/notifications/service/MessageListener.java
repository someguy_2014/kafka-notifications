package com.example.notifications.service;

import com.example.notifications.common.Constants;
import com.example.notifications.model.Message;
import org.springframework.kafka.annotation.KafkaListener;

public class MessageListener {

    @KafkaListener(topics = "sms", containerFactory = "messageKafkaListenerContainerFactory")
    public void smsListener(Message message) {
        System.out.println("Received sms message: " + message);

        MessageSender.sendMessage(Constants.BY_SMS, message);
    }

    @KafkaListener(topics = "email", containerFactory = "messageKafkaListenerContainerFactory")
    public void emailListener(Message message) {
        System.out.println("Received e-mail message: " + message);

        MessageSender.sendMessage(Constants.BY_EMAIL, message);
    }

    @KafkaListener(topics = "slack", containerFactory = "messageKafkaListenerContainerFactory")
    public void slackListener(Message message) {
        System.out.println("Received slack message: " + message);

        MessageSender.sendMessage(Constants.BY_SLACK, message);
    }
}
