package com.example.notifications.service;

import com.example.notifications.common.Constants;
import com.example.notifications.model.Message;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.function.Consumer;

public class MessageProducer {

    private final KafkaTemplate<String, Message> messageKafkaTemplate;
    private final Map<String, Consumer<Message>> messageActions = Map.of(
            Constants.BY_SMS, this::sendBySms,
            Constants.BY_EMAIL, this::sendByEmail,
            Constants.BY_SLACK, this::sendBySlack
    );

    public MessageProducer(KafkaTemplate<String, Message> messageKafkaTemplate){
        this.messageKafkaTemplate = messageKafkaTemplate;
    }

    @Transactional
    public void send(String type, Message message) {
        messageActions.get(type).accept(message);
    }

    private void sendBySms(Message message) {
        messageKafkaTemplate.send(Constants.BY_SMS, message);
    }

    private void sendByEmail(Message message) {
        messageKafkaTemplate.send(Constants.BY_EMAIL, message);
    }

    private void sendBySlack(Message message) {
        messageKafkaTemplate.send(Constants.BY_SLACK, message);
    }
}
