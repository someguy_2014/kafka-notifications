package com.example.notifications.service;

import com.example.notifications.common.Constants;
import com.example.notifications.model.Message;

import java.util.Map;

public class MessageSender {

    private static MessageSender messageSender;
    private final Map<String, Sender> messageSenders = Map.of(
            Constants.BY_SMS, new SmsSender(),
            Constants.BY_EMAIL, new EmailSender(),
            Constants.BY_SLACK, new SlackSender()
    );

    public static void sendMessage(String type, Message message){
        if (messageSender == null)
            messageSender = new MessageSender();

        messageSender.send(type, message);
    }

    private void send(String type, Message message){
        messageSenders.get(type).send(message);
    }

    private interface Sender {
        void send(Message message);
    }

    private class SmsSender implements Sender{

        @Override
        public void send(Message message) {
            System.err.println("Send by SMS: " + message);
        }
    }

    private class EmailSender implements Sender{

        @Override
        public void send(Message message) {
            System.err.println("Send by E-mail: " + message);
        }
    }

    private class SlackSender implements Sender{

        @Override
        public void send(Message message) {
            System.err.println("Send by Slack: " + message);
        }
    }
}
