package com.example.notifications.common;

public class Constants {

    public static final String BY_SMS = "sms";
    public static final String BY_EMAIL = "email";
    public static final String BY_SLACK = "slack";

}
