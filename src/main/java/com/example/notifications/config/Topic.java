package com.example.notifications.config;

import com.example.notifications.common.Constants;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Topic {

    @Bean
    public NewTopic sms() {
        return new NewTopic(Constants.BY_SMS, 1, Short.valueOf("1"));
    }

    @Bean
    public NewTopic email() {
        return new NewTopic(Constants.BY_EMAIL, 1, Short.valueOf("1"));
    }

    @Bean
    public NewTopic slack() {
        return new NewTopic(Constants.BY_SLACK, 1, Short.valueOf("1"));
    }
}
