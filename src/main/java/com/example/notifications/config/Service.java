package com.example.notifications.config;

import com.example.notifications.model.Message;
import com.example.notifications.service.MessageListener;
import com.example.notifications.service.MessageProducer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;

@Configuration
public class Service {

    @Bean
    public MessageProducer messageProducer(KafkaTemplate<String, Message> messageKafkaTemplate) {
        return new MessageProducer(messageKafkaTemplate);
    }

    @Bean
    public MessageListener messageListener() {
        return new MessageListener();
    }
}
