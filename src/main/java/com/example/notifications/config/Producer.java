package com.example.notifications.config;

import com.example.notifications.model.Message;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.transaction.KafkaTransactionManager;

import java.util.UUID;

@Configuration
public class Producer {

    @Bean
    KafkaTransactionManager<String, Message> kafkaTransactionManager(ProducerFactory producerFactory) {
        return new KafkaTransactionManager<>(producerFactory);
    }

    @Bean
    public KafkaTemplate<String, Message> kafkaTemplate(ProducerFactory producerFactory) {
        KafkaTemplate kafkaTemplate = new KafkaTemplate<>(producerFactory);

        kafkaTemplate.setTransactionIdPrefix(String.format("tx-%s-", UUID.randomUUID()));

        return kafkaTemplate;
    }

}
