package com.example.notifications.model;

public class Message {

    private final String from;
    private final String to;
    private final String message;

    public Message(){
        this("", "", "");
    }

    public Message(String from, String to, String message){
        this.from = from;
        this.to = to;
        this.message = message;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "Message{" +
                "from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
